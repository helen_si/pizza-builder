import React, { Component } from 'react';
import LoginComponent from './LoginComponent/LoginComponent';
import RegistrationComponent from './RegistrationComponent/RegistrationComponent';

class Auth extends Component {
  state = {
    isLoginProcess: true
  };

  switchAuthProcessHandler = () => {
    const isLoginProcess = !this.state.isLoginProcess;
    this.setState({ isLoginProcess });
  };

  render() {
    const { isLoginProcess } = this.state;
    const { shouldReset, onCancel } = this.props;
    return (
      <>
        {isLoginProcess ? (
          <LoginComponent
            shouldReset={shouldReset}
            onCancel={onCancel}
            switchAuthProcess={this.switchAuthProcessHandler}
          />
        ) : (
          <RegistrationComponent
            shouldReset={shouldReset}
            onCancel={onCancel}
            switchAuthProcess={this.switchAuthProcessHandler}
          />
        )}
      </>
    );
  }
}

export default Auth;
