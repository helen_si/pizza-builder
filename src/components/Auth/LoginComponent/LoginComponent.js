import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../../UI/Spinner/Spinner';
import * as actions from '../../../store/actions';
import '../Auth.css';

class LoginComponent extends Component {
  state = {
    email: '',
    password: '',
    validationErrors: {}
  };

  componentDidUpdate(prevProps) {
    const { user, error, onCancel, shouldReset } = this.props;
    if (error && error !== prevProps.error) {
      const validationErrors = {};
      if (error.code === 'auth/user-not-found') {
        validationErrors.email = error.message;
      }
      if (error.code === 'auth/wrong-password') {
        validationErrors.password = error.message;
      }
      this.setState({ validationErrors });
    }
    if (shouldReset && shouldReset !== prevProps.shouldReset) {
      this.setState({
        email: '',
        password: '',
        validationErrors: {}
      });
    }
    if (user && user !== prevProps.user) {
      if (user.isAdmin) {
        this.props.history.push('/admin-page/all-orders');
      }
      onCancel();
    }
  }

  loginHandler = e => {
    e.preventDefault();
    const { loginUser } = this.props;
    const { email, password } = this.state;
    const validationErrors = {};
    const emptyFields = ['email', 'password'];
    emptyFields.forEach(field => {
      if (!this.state[field]) {
        validationErrors[field] = 'This field is required';
      }
    });
    if (this.state.email && this.state.email.indexOf('@') === -1) {
      validationErrors.email = 'Enter valid email';
    }
    this.setState({ validationErrors });
    if (Object.keys(validationErrors).length) {
      return;
    }
    loginUser(email, password);
  };

  render() {
    const { email, password, validationErrors } = this.state;
    const { switchAuthProcess, isLoadingUser } = this.props;
    return (
      <>
        {isLoadingUser ? (
          <div className="LoaderContainerLogin">
            <Spinner />
          </div>
        ) : (
          <form className="AuthForm">
            <p>Login</p>
            <input
              style={
                validationErrors.email ? { border: '1px solid red' } : null
              }
              type="email"
              placeholder="Email"
              value={email}
              onChange={e => {
                this.setState({ email: e.target.value });
              }}
            />
            {validationErrors.email && (
              <div className="Errors">{validationErrors.email}</div>
            )}
            <input
              style={
                validationErrors.password ? { border: '1px solid red' } : null
              }
              type="password"
              placeholder="Password"
              value={password}
              onChange={e => {
                this.setState({ password: e.target.value });
              }}
            />
            {validationErrors.password && (
              <div className="Errors">{validationErrors.password}</div>
            )}
            <div className="BtnsAuth">
              <button
                className="Btn UserBtnColor"
                type="submit"
                onClick={this.loginHandler}
              >
                Sign In
              </button>
              <button
                className="Btn UserBtnColor"
                type="button"
                onClick={switchAuthProcess}
              >
                New customer?
              </button>
            </div>
          </form>
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    error: state.auth.error,
    isLoadingUser: state.auth.isLoadingUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loginUser: (email, password) => dispatch(actions.loginUser(email, password))
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(LoginComponent)
);
