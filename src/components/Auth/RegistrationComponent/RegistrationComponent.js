import React, { Component } from 'react';
import { connect } from 'react-redux';
import Spinner from '../../UI/Spinner/Spinner';
import * as actions from '../../../store/actions';
import '../Auth.css';

class RegistrationComponent extends Component {
  state = {
    name: '',
    email: '',
    password: '',
    repeatedPassword: '',
    validationErrors: {}
  };

  componentDidUpdate(prevProps) {
    const { user, error, onCancel, shouldReset } = this.props;
    if (error && error !== prevProps.error) {
      const validationErrors = {};
      if (error.code === 'auth/email-already-in-use') {
        validationErrors.email = error.message;
      }
      if (error.code === 'auth/weak-password') {
        validationErrors.password = error.message;
        validationErrors.repeatedPassword = error.message;
      }
      this.setState({ validationErrors });
    }
    if (shouldReset && shouldReset !== prevProps.shouldReset) {
      this.setState({
        name: '',
        email: '',
        password: '',
        repeatedPassword: '',
        validationErrors: {}
      });
    }
    if (user) {
      onCancel();
    }
  }

  registerHandler = e => {
    e.preventDefault();
    const { registerUser } = this.props;
    const { name, email, password, repeatedPassword } = this.state;
    const validationErrors = {};
    const emptyFields = ['name', 'email', 'password', 'repeatedPassword'];
    emptyFields.forEach(field => {
      if (!this.state[field]) {
        validationErrors[field] = 'This field is required';
      }
    });
    if (this.state.email && this.state.email.indexOf('@') === -1) {
      validationErrors.email = 'Enter valid email';
    }
    if (password !== repeatedPassword) {
      validationErrors.password = 'Passwords do not match';
      validationErrors.repeatedPassword = 'Passwords do not match';
    }
    this.setState({ validationErrors });
    if (Object.keys(validationErrors).length) {
      return;
    }
    registerUser(name, email, password);
  };

  render() {
    const {
      name,
      email,
      password,
      repeatedPassword,
      validationErrors
    } = this.state;
    const { switchAuthProcess, isLoadingUser } = this.props;
    return (
      <>
        {isLoadingUser ? (
          <div className="LoaderContainerRegistration">
            <Spinner />
          </div>
        ) : (
          <form className="AuthForm">
            <p>Registration</p>
            <input
              style={validationErrors.name ? { border: '1px solid red' } : null}
              type="text"
              placeholder="Name"
              value={name}
              onChange={e => {
                this.setState({ name: e.target.value });
              }}
            />
            {validationErrors.name && (
              <div className="Errors">{validationErrors.name}</div>
            )}
            <input
              style={
                validationErrors.email ? { border: '1px solid red' } : null
              }
              type="email"
              placeholder="Email"
              value={email}
              onChange={e => {
                this.setState({ email: e.target.value });
              }}
            />
            {validationErrors.email && (
              <div className="Errors">{validationErrors.email}</div>
            )}
            <input
              style={
                validationErrors.password ? { border: '1px solid red' } : null
              }
              type="password"
              placeholder="Password"
              value={password}
              onChange={e => {
                this.setState({ password: e.target.value });
              }}
            />
            {validationErrors.password && (
              <div className="Errors">{validationErrors.password}</div>
            )}
            <input
              style={
                validationErrors.repeatedPassword
                  ? { border: '1px solid red' }
                  : null
              }
              type="password"
              placeholder="Repeat password"
              value={repeatedPassword}
              onChange={e =>
                this.setState({ repeatedPassword: e.target.value })
              }
            />
            {validationErrors.repeatedPassword && (
              <div className="Errors">{validationErrors.repeatedPassword}</div>
            )}
            <div className="BtnsAuth">
              <button
                className="Btn UserBtnColor"
                type="submit"
                onClick={this.registerHandler}
              >
                Sign Up
              </button>
              <button
                className="Btn UserBtnColor"
                type="button"
                onClick={switchAuthProcess}
              >
                Already have an account?
              </button>
            </div>
          </form>
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    error: state.auth.error,
    isLoadingUser: state.auth.isLoadingUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    registerUser: (name, email, password) =>
      dispatch(actions.registerUser(name, email, password))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegistrationComponent);
