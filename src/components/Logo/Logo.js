import React from 'react';
import { Link } from 'react-router-dom';
import PizzaLogo from '../../assets/images/logo.png';
import './Logo.css';

const logo = props => {
  const { userIsAdmin } = props;
  return (
    <div className="Logo">
      <Link to={userIsAdmin ? '/admin-page' : '/'}>
        <img src={PizzaLogo} alt="Pizza-logo" />
      </Link>
    </div>
  );
};

export default logo;
