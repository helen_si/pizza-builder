import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import Logo from '../Logo/Logo';
import Modal from '../UI/Modal/Modal';
import Auth from '../Auth/Auth';
import * as actions from '../../store/actions';
import { connect } from 'react-redux';
import './Navbar.css';

class Navbar extends Component {
  state = {
    isAuth: false
  };

  showLoginFormHandler = () => {
    this.setState({ isAuth: true });
  };

  cancelLoginFormHandler = () => {
    this.setState({ isAuth: false });
  };

  logoutHandler = () => {
    const { logoutUser } = this.props;
    this.props.history.push('/');
    logoutUser();
  };

  render() {
    const { user } = this.props;
    const { isAuth } = this.state;
    let navbarBtns = null;
    if (!user) {
      navbarBtns = (
        <li onClick={this.showLoginFormHandler}>
          <NavLink to="#" activeClassName="UserActive">
            Login
          </NavLink>
        </li>
      );
    } else {
      navbarBtns = (
        <>
          <li>
            <NavLink to={`/orders/${user.uid}`} activeClassName="UserActive">
              Orders
            </NavLink>
          </li>
          <li onClick={this.logoutHandler}>
            <NavLink to="#" activeClassName="UserActive">
              Logout
            </NavLink>
          </li>
        </>
      );
    }
    return (
      <div
        className={
          user && user.isAdmin
            ? 'Navbar AdminNavbarColor'
            : 'Navbar UsernNavbarColor'
        }
      >
        <Modal visible={isAuth} onCancel={this.cancelLoginFormHandler}>
          <Auth shouldReset={isAuth} onCancel={this.cancelLoginFormHandler} />
        </Modal>
        <Logo userIsAdmin={user && user.isAdmin} />
        <ul
          className={
            user && user.isAdmin ? 'NavLinks AdminLinks' : 'NavLinks UserLinks'
          }
        >
          {user && user.isAdmin ? (
            <>
              <li>
                <NavLink to="/admin-page" exact activeClassName="AdminActive">
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink
                  to="/admin-page/pizza-builder"
                  activeClassName="AdminActive"
                >
                  Pizza Builder
                </NavLink>
              </li>
              <li>
                <NavLink
                  to={'/admin-page/all-orders'}
                  activeClassName="AdminActive"
                >
                  All orders
                </NavLink>
              </li>
              <li onClick={this.logoutHandler}>
                <NavLink to="#" activeClassName="AdminActive">
                  Logout
                </NavLink>
              </li>
            </>
          ) : (
            <>
              <li>
                <NavLink to="/" exact activeClassName="UserActive">
                  Home
                </NavLink>
              </li>
              <li>
                <NavLink to="/pizza-builder" activeClassName="UserActive">
                  Pizza Builder
                </NavLink>
              </li>
              <li>
                <NavLink to="/contacts" activeClassName="UserActive">
                  Contact Us
                </NavLink>
              </li>
              {navbarBtns}
            </>
          )}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logoutUser: () => dispatch(actions.logoutProcess())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar);
