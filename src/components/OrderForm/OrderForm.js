import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Spinner from '../../components/UI/Spinner/Spinner';
import * as actions from '../../store/actions';
import './OrderForm.css';
import moment from 'moment';

class OrderForm extends Component {
  state = {
    name: '',
    email: '',
    phoneNumber: '',
    address: '',
    paymentMethod: '',
    validationErrors: {},
    success: false,
    newOrder: null
  };

  componentDidUpdate(prevProps) {
    const { user, shouldReset, newOrder } = this.props;
    const name = user && !user.isAdmin ? user.name : '';
    const email = user && !user.isAdmin ? user.email : '';
    const phoneNumber =
      user && !user.isAdmin && user.phoneNumber ? user.phoneNumber : '';
    const address = user && !user.isAdmin && user.address ? user.address : '';
    if (newOrder && newOrder !== prevProps.newOrder) {
      this.setState({ success: true });
    }
    if (shouldReset && shouldReset !== prevProps.shouldReset) {
      this.setState({
        name,
        email,
        phoneNumber,
        address,
        paymentMethod: '',
        validationErrors: {},
        success: false
      });
    }
  }

  createOrderHandler = e => {
    e.preventDefault();
    const {
      user,
      ingredients,
      sauces,
      totalPrice,
      order,
      createOrder
    } = this.props;
    const { name, email, phoneNumber, address, paymentMethod } = this.state;
    const userId = user && !user.isAdmin ? user.uid : null;
    const validationErrors = {};
    const emptyFields = [
      'name',
      'email',
      'phoneNumber',
      'address',
      'paymentMethod'
    ];
    emptyFields.forEach(field => {
      if (!this.state[field]) {
        validationErrors[field] = 'This field is required';
      }
    });
    if (this.state.email && this.state.email.indexOf('@') === -1) {
      validationErrors.email = 'Enter valid email';
    }
    this.setState({ validationErrors });
    if (Object.keys(validationErrors).length) {
      return;
    } else {
      let pizzaIngredients = {};
      let pizzaSauce = '';
      let orderPrice = '';
      if (!order) {
        ingredients
          .filter(ingredient => ingredient.amount !== 0)
          .forEach(item => (pizzaIngredients[item.name] = item.amount));
        pizzaSauce = sauces.filter(sauce => sauce.selected)[0].name;
        orderPrice = totalPrice;
      } else {
        pizzaIngredients = {
          ...order.ingredients
        };
        pizzaSauce = order.sauce;
        orderPrice = order.totalPrice;
      }
      const date = moment().format();
      const newOrder = {
        user: {
          name,
          email,
          phoneNumber,
          address,
          paymentMethod
        },
        ingredients: pizzaIngredients,
        sauce: pizzaSauce,
        totalPrice: orderPrice,
        userId,
        date,
        status: 'new'
      };
      createOrder(newOrder, name, phoneNumber, address);
    }
  };

  render() {
    const {
      name,
      email,
      phoneNumber,
      address,
      validationErrors,
      success
    } = this.state;
    const { user, isCreatingOrder } = this.props;
    const redirectLink =
      user && user.isAdmin ? '/admin-page/created-order' : '/created-order';
    let orderForm = null;
    if (success) {
      orderForm = <Redirect to={{ pathname: redirectLink }} />;
    } else {
      orderForm = (
        <form className="OrderForm">
          <p>Your Contact Data</p>
          <input
            style={validationErrors.name ? { border: '1px solid red' } : null}
            type="text"
            placeholder="Name"
            value={name}
            onChange={e => {
              this.setState({ name: e.target.value });
            }}
          />
          {validationErrors.name && (
            <div className="Errors">{validationErrors.name}</div>
          )}
          <input
            style={validationErrors.email ? { border: '1px solid red' } : null}
            type="email"
            placeholder="Email"
            value={email}
            onChange={e => {
              this.setState({ email: e.target.value });
            }}
          />
          {validationErrors.email && (
            <div className="Errors">{validationErrors.email}</div>
          )}
          <input
            style={
              validationErrors.phoneNumber ? { border: '1px solid red' } : null
            }
            type="text"
            placeholder="Phone Number"
            value={phoneNumber}
            onChange={e => {
              this.setState({ phoneNumber: e.target.value });
            }}
          />
          {validationErrors.phoneNumber && (
            <div className="Errors">{validationErrors.phoneNumber}</div>
          )}
          <input
            style={
              validationErrors.address ? { border: '1px solid red' } : null
            }
            type="text"
            placeholder="Address"
            value={address}
            onChange={e => {
              this.setState({ address: e.target.value });
            }}
          />
          {validationErrors.address && (
            <div className="Errors">{validationErrors.address}</div>
          )}
          <select
            style={
              validationErrors.paymentMethod
                ? { border: '1px solid red' }
                : null
            }
            defaultValue="Payment method"
            onChange={e => {
              this.setState({ paymentMethod: e.target.value });
            }}
          >
            <option disabled>Payment method</option>
            <option value="Cash">Cash</option>
            <option value="Credit Card">Credit Card</option>
          </select>
          {validationErrors.paymentMethod && (
            <div className="Errors">{validationErrors.paymentMethod}</div>
          )}
          <button
            className={
              user && user.isAdmin ? 'Btn AdminBtnColor' : 'Btn UserBtnColor'
            }
            type="submit"
            onClick={this.createOrderHandler}
          >
            Submit
          </button>
        </form>
      );
    }
    return (
      <>
        {isCreatingOrder ? (
          <div className="LoaderContainer LoaderContainerOrder">
            <Spinner />
          </div>
        ) : (
          orderForm
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    ingredients: state.pizzaData.ingredients,
    sauces: state.pizzaData.sauces,
    totalPrice: state.pizzaData.totalPrice,
    isCreatingOrder: state.orders.isCreatingOrder,
    order: state.orders.order,
    newOrder: state.orders.newOrder
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createOrder: (newOrder, name, phoneNumber, address) =>
      dispatch(actions.createOrder(newOrder, name, phoneNumber, address))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderForm);
