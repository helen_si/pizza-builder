import React from 'react';
import './Pizza.css';

const pizza = props => {
  const { ingredients, sauces } = props;
  let ingredientsIcons = [];
  ingredients.forEach(item => {
    if (item.amount > 0) {
      ingredientsIcons.push(
        item.name.charAt(0).toUpperCase() + item.name.slice(1)
      );
    }
  });
  let selectedSauce = sauces.find(item => item.selected);
  selectedSauce =
    selectedSauce &&
    selectedSauce.name
      .split(' ')[0]
      .charAt(0)
      .toUpperCase() + selectedSauce.name.split(' ')[0].slice(1);
  return (
    <div className="PizzaContainer">
      <div className="PizzaBorder PizzaLayoutPosition">
        <div className="PizzaCenter PizzaLayoutPosition">
          <div className={`Sauce ${selectedSauce}`}>
            <div className={`FirstIng ${ingredientsIcons[0]}`} />
            <div className={`SecondIng ${ingredientsIcons[1]}`} />
            <div className={`ThirdIng ${ingredientsIcons[2]}`} />
            <div className={`FourthIng ${ingredientsIcons[3]}`} />
            <div className={`FifthIng ${ingredientsIcons[4]}`} />
            <div className={`SixthIng ${ingredientsIcons[5]}`} />
            <div className={`SeventhIng ${ingredientsIcons[6]}`} />
            <div className={`EighthIng ${ingredientsIcons[7]}`} />
            <div className={`NinthIng ${ingredientsIcons[8]}`} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default pizza;
