import React from 'react';
import './PizzaItem.css';

const pizzaItem = props => {
  const {
    userIsAdmin,
    name,
    isSauce,
    price,
    amount,
    isDisabledMin,
    isDisabledMax,
    removeIngredient,
    addIngredient,
    chooseSauce,
    selected
  } = props;
  let pizzaItem = null;
  if (!isSauce) {
    pizzaItem = (
      <div className="Ingredient">
        <div className="IngredientName">
          {name.charAt(0).toUpperCase() + name.slice(1)}
        </div>
        <div className="IngredientPrice">${price.toFixed(2)}</div>
        <div className="BuilderControls">
          <button
            className={
              userIsAdmin
                ? 'Control ControlForAdmin RemoveControl'
                : 'Control ControlForUser RemoveControl'
            }
            value="remove"
            onClick={removeIngredient}
            disabled={isDisabledMin}
          >
            -
          </button>
          {amount}
          <button
            className={
              userIsAdmin
                ? 'Control ControlForAdmin AddControl'
                : 'Control ControlForUser AddControl'
            }
            value="add"
            onClick={addIngredient}
            disabled={isDisabledMax}
          >
            +
          </button>
        </div>
      </div>
    );
  } else {
    pizzaItem = (
      <div className="SauceIngredient">
        <input
          type="radio"
          value={name}
          name="sauce"
          onClick={chooseSauce}
          defaultChecked={selected}
        />
        {name.charAt(0).toUpperCase() + name.slice(1)}
      </div>
    );
  }
  return <>{pizzaItem}</>;
};

export default pizzaItem;
