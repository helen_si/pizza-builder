import React from 'react';
import PizzaItem from './PizzaItem/PizzaItem';
import './PizzaItems.css';

const pizzaItems = props => {
  const {
    userIsAdmin,
    ingredients,
    sauces,
    totalPrice,
    basicPrice,
    showOrderForm,
    chooseSauce,
    addIngredient,
    removeIngredient,
    showPizzaTemplateForm
  } = props;
  return (
    <div className="PizzaItems">
      <div className="Ingredients">
        {ingredients.map((item, index) => (
          <PizzaItem
            key={index}
            name={item.name}
            isSauce={item.isSauce}
            price={item.price}
            amount={item.amount}
            isDisabledMin={item.amount === 0}
            isDisabledMax={item.amount === 3}
            userIsAdmin={userIsAdmin}
            addIngredient={() => addIngredient(item.name)}
            removeIngredient={() => removeIngredient(item.name)}
          />
        ))}
      </div>
      <div className="Sauces">
        {sauces.map((item, index) => (
          <PizzaItem
            key={index}
            name={item.name}
            isSauce={item.isSauce}
            selected={item.selected}
            chooseSauce={() => chooseSauce(item.name)}
          />
        ))}
      </div>
      <div className="TotalPriceBlock">
        Total price: ${totalPrice && totalPrice.toFixed(2)}
      </div>
      {userIsAdmin ? (
        <div className="BtnContainer">
          <button
            className="Btn AdminBtnColor"
            value="order"
            disabled={totalPrice === basicPrice}
            onClick={showOrderForm}
          >
            Create Order
          </button>
          <button
            className="Btn AdminBtnColor"
            value="order"
            disabled={totalPrice === basicPrice}
            onClick={showPizzaTemplateForm}
          >
            Create pizza
          </button>
        </div>
      ) : (
        <button
          className="Btn UserBtnColor"
          value="order"
          disabled={totalPrice === basicPrice}
          onClick={showOrderForm}
        >
          Order Now
        </button>
      )}
    </div>
  );
};

export default pizzaItems;
