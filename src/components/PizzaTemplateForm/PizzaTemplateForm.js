import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Spinner from '../../components/UI/Spinner/Spinner';
import * as actions from '../../store/actions';
import './PizzaTemplateForm.css';

class PizzaTemplateForm extends Component {
  state = {
    templateName: '',
    ingredients: '',
    sauce: '',
    totalPrice: '',
    validationErrors: {},
    success: false,
    newPizzaTemplate: null
  };

  componentDidMount() {
    const { ingredients, sauces, totalPrice } = this.props;
    const pizzaTemplateIngredients = [];
    ingredients.forEach(ingredient => {
      if (ingredient.amount > 0) {
        pizzaTemplateIngredients.push({
          name: ingredient.name,
          amount: ingredient.amount,
          isSauce: ingredient.isSauce
        });
      }
    });
    const pizzaTemplateSauce = [];
    sauces.forEach(sauce => {
      if (sauce.selected) {
        pizzaTemplateSauce.push({
          name: sauce.name,
          isSauce: sauce.isSauce
        });
      }
    });
    this.setState({
      ingredients: pizzaTemplateIngredients,
      sauce: pizzaTemplateSauce,
      totalPrice
    });
  }

  componentDidUpdate(prevProps) {
    const {
      ingredients,
      sauces,
      totalPrice,
      pizzaTemplate,
      shouldReset
    } = this.props;
    if (pizzaTemplate && pizzaTemplate !== prevProps.pizzaTemplate) {
      this.setState({ success: true });
    }
    if (shouldReset && shouldReset !== prevProps.shouldReset) {
      this.setState({
        templateName: '',
        validationErrors: {},
        success: false
      });
    }
    if (ingredients !== prevProps.ingredients || sauces !== prevProps.sauces) {
      const pizzaTemplateIngredients = [];
      ingredients.forEach(ingredient => {
        if (ingredient.amount > 0) {
          pizzaTemplateIngredients.push({
            name: ingredient.name,
            amount: ingredient.amount,
            isSauce: ingredient.isSauce
          });
        }
      });
      const pizzaTemplateSauce = [];
      sauces.forEach(sauce => {
        if (sauce.selected) {
          pizzaTemplateSauce.push({
            name: sauce.name,
            isSauce: sauce.isSauce
          });
        }
      });
      this.setState({
        ingredients: pizzaTemplateIngredients,
        sauce: pizzaTemplateSauce,
        totalPrice
      });
    }
  }

  createPizzaTemplateHandler = e => {
    e.preventDefault();
    const { createPizzaTemplate } = this.props;
    const { templateName, ingredients, sauce } = this.state;
    const validationErrors = {};
    const emptyFields = ['templateName'];
    emptyFields.forEach(field => {
      if (!this.state[field]) {
        validationErrors[field] = 'This field is required';
      }
    });
    this.setState({ validationErrors });
    if (Object.keys(validationErrors).length) {
      return;
    } else {
      const newPizzaTemplate = {
        templateName,
        ingredients,
        sauce
      };
      createPizzaTemplate(newPizzaTemplate);
    }
  };

  render() {
    const { isCreatingPizzaTemplate } = this.props;
    const {
      templateName,
      ingredients,
      sauce,
      totalPrice,
      validationErrors,
      success
    } = this.state;
    let pizzaTemplateForm = null;
    if (success) {
      pizzaTemplateForm = <Redirect to="/admin-page" />;
    } else {
      pizzaTemplateForm = (
        <form className="PizzaTemplateForm">
          <p>New pizza template</p>
          <input
            style={
              validationErrors.templateName ? { border: '1px solid red' } : null
            }
            type="text"
            placeholder="Name"
            value={templateName}
            onChange={e => {
              this.setState({ templateName: e.target.value });
            }}
          />
          {validationErrors.templateName && (
            <div className="Errors">{validationErrors.templateName}</div>
          )}
          <div className="PizzaTemplateInfo">
            Pizza with{' '}
            {Object.keys(ingredients).map((ingredient, index) => {
              const separator = index !== ingredients.length - 1 ? ', ' : ' ';
              return (
                <div key={index} style={{ fontWeight: 'bold' }}>
                  {ingredients[ingredient].name}(
                  {ingredients[ingredient].amount}){separator}
                </div>
              );
            })}
            and{' '}
            {Object.keys(sauce).map(sauceKey => {
              return (
                <span key={sauceKey} style={{ fontWeight: 'bold' }}>
                  {sauce[sauceKey].name}.
                </span>
              );
            })}
            <div style={{ fontWeight: 'bold', marginTop: '20px' }}>
              Total Price: ${totalPrice && totalPrice.toFixed(2)}
            </div>
          </div>
          <button
            className="Btn AdminBtnColor"
            type="submit"
            onClick={this.createPizzaTemplateHandler}
          >
            Submit
          </button>
        </form>
      );
    }
    return (
      <>
        {isCreatingPizzaTemplate ? (
          <div className="LoaderContainer">
            <Spinner />
          </div>
        ) : (
          pizzaTemplateForm
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    ingredients: state.pizzaData.ingredients,
    sauces: state.pizzaData.sauces,
    totalPrice: state.pizzaData.totalPrice,
    isCreatingPizzaTemplate: state.pizzaData.isCreatingPizzaTemplate,
    pizzaTemplate: state.pizzaData.pizzaTemplate
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createPizzaTemplate: pizzaTemplate =>
      dispatch(actions.createPizzaTemplate(pizzaTemplate))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PizzaTemplateForm);
