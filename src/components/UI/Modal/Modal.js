import React from 'react';
import './Modal.css';

const modal = props => {
  const { onCancel, visible, children } = props;
  return (
    <>
      {props.visible && <div className="Backdrop" onClick={onCancel} />}
      <div
        className="Modal"
        style={{
          transform: visible ? 'translateY(0)' : 'translateY(-100vh)',
          opacity: visible ? '1' : '0'
        }}
      >
        {children}
      </div>
    </>
  );
};

export default modal;
