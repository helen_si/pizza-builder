import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Spinner.css';

class Spinner extends Component {
  render() {
    const { user } = this.props;
    return (
      <div
        className={
          user && user.isAdmin ? 'Loader AdminLoader' : 'Loader UserLoader'
        }
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  };
};

export default connect(
  mapStateToProps,
  null
)(Spinner);
