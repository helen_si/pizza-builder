import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/';
import './Contacts.css';
import Helmet from 'react-helmet';
import Spinner from '../../components/UI/Spinner/Spinner';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPhone,
  faGlobeEurope,
  faEnvelope
} from '@fortawesome/free-solid-svg-icons';

class Contacts extends Component {
  componentDidMount() {
    const { getCompanyInfo } = this.props;
    getCompanyInfo();
  }

  render() {
    const { companyInfo, isLoadingCompanyInfo } = this.props;
    let contactInfo = null;
    if (companyInfo) {
      contactInfo = (
        <div className="ContactsContainer">
          <iframe
            title="map"
            src={companyInfo.mapUrl}
            width="700"
            height="500"
            frameBorder="0"
            style={{ border: 0 }}
            allowFullScreen
          />
          <div className="Contacts">
            <div className="InfoContainer">
              <FontAwesomeIcon icon={faPhone} size="lg" />
              <div className="Info">
                <h3>Contact us:</h3>
                <p>{companyInfo.mainPhone}</p>
                <p>{companyInfo.phone}</p>
              </div>
            </div>
            <div className="InfoContainer">
              <FontAwesomeIcon icon={faGlobeEurope} size="lg" />
              <div className="Info">
                <h3>Our location:</h3>
                <p>{companyInfo.location}</p>
              </div>
            </div>
            <div className="InfoContainer">
              <FontAwesomeIcon icon={faEnvelope} size="lg" />
              <div className="Info">
                <h3>Email:</h3>
                <p>{companyInfo.email}</p>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return (
      <>
        <Helmet>
          <title>Contacts</title>
        </Helmet>
        {isLoadingCompanyInfo ? (
          <div className="LoaderContainer">
            <Spinner />
          </div>
        ) : (
          contactInfo
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    companyInfo: state.companyInfo.companyInfo,
    isLoadingCompanyInfo: state.companyInfo.isLoadingCompanyInfo
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getCompanyInfo: () => dispatch(actions.getCompanyInfo())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contacts);
