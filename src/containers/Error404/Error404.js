import React, { Component } from 'react';
import './Error404.css';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class Error404 extends Component {
  render() {
    const { user } = this.props;
    return (
      <>
        <Helmet>
          <title>Oooops!</title>
        </Helmet>
        <div className="Error404">
          <h1>Sorry, page not found!</h1>
          <div className="HomePageLink">
            {user && user.isAdmin ? (
              <Link to="/admin-page">
                <button className="Btn AdminBtnColor">Home Page</button>
              </Link>
            ) : (
              <Link to="/">
                <button className="Btn UserBtnColor">Home Page</button>
              </Link>
            )}
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  };
};

export default connect(
  mapStateToProps,
  null
)(Error404);
