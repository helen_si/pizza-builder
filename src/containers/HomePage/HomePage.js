import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import Spinner from '../../components/UI/Spinner/Spinner';
import PizzaLogo from '../../assets/images/logo.png';
import './HomePage.css';

class HomePage extends Component {
  componentDidMount() {
    const { getPizzaTemplates } = this.props;
    getPizzaTemplates();
  }

  render() {
    const { isLoadingPizzaData, pizzaTemplates, user } = this.props;
    let pizzaTemplatesData = [];
    if (pizzaTemplates) {
      Object.keys(pizzaTemplates).forEach(key => {
        pizzaTemplates[key] = {
          ...pizzaTemplates[key],
          key
        };
        pizzaTemplatesData.push(pizzaTemplates[key]);
      });
    }
    const link = user && user.isAdmin ? '/admin-page/' : '/';
    let pizzaTemplatesDetails = null;
    if (pizzaTemplates) {
      pizzaTemplatesDetails = (
        <>
          <h2>Or if you need ready solution...</h2>
          <div className="PizzaTemplates">
            {pizzaTemplatesData.map(pizzaTemplate => {
              return (
                <Link
                  to={{
                    pathname: link + `pizza-builder/${pizzaTemplate.key}`,
                    state: { pizzaTemplate }
                  }}
                  key={pizzaTemplate.key}
                >
                  <div className="PizzaTemplate">
                    <img src={PizzaLogo} alt="Pizza-logo" />
                    <h4>{pizzaTemplate.templateName}</h4>
                  </div>
                </Link>
              );
            })}
          </div>
        </>
      );
    }
    return (
      <>
        <Helmet>
          <title>Pizza Builder</title>
        </Helmet>
        <div className="HomePage">
          <h1>Would you like some yummy pizza?</h1>
          <div className="ToPizzaBuilder">
            <Link to={link + 'pizza-builder'}>
              <div className="ToPizzaBuilderBtn">
                <img src={PizzaLogo} alt="pizza-logo" />
                <div>Go to Pizza Builder</div>
              </div>
            </Link>
          </div>
          {isLoadingPizzaData ? (
            <div className="LoaderContainer LoaderContainerTemplates">
              <Spinner />
            </div>
          ) : (
            pizzaTemplatesDetails
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoadingPizzaData: state.pizzaData.isLoadingPizzaData,
    pizzaTemplates: state.pizzaData.pizzaTemplates,
    user: state.auth.user
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getPizzaTemplates: () => dispatch(actions.getPizzaTemplates())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomePage);
