import React, { Component } from 'react';
import { Switch, Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import Navbar from '../../components/Navbar/Navbar';
import HomePage from '../HomePage/HomePage';
import PizzaBuilder from '../PizzaBuilder/PizzaBuilder';
import Orders from '../Orders/Orders';
import Order from '../Orders/Order/Order';
import Contacts from '../Contacts/Contacts';
import OrderSuccessPage from '../OrderSuccessPage/OrderSuccessPage';
import Error404 from '../Error404/Error404';
import AuthCheck from '../AuthCheck/AuthCheck';
import './Layout.css';

class Layout extends Component {
  render() {
    const { isAuthCheckCompleted, user } = this.props;
    return (
      <>
        {!isAuthCheckCompleted ? (
          <AuthCheck />
        ) : (
          <>
            <Route component={Navbar} />
            <div className="Layout">
              {user && user.isAdmin ? (
                <Switch>
                  <Route path="/admin-page" exact component={HomePage} />
                  <Route
                    path="/admin-page/pizza-builder"
                    exact
                    component={PizzaBuilder}
                  />
                  <Route
                    path="/admin-page/pizza-builder/:pizzaTemplate/"
                    exact
                    component={PizzaBuilder}
                  />
                  <Route
                    path="/admin-page/all-orders"
                    exact
                    component={Orders}
                  />
                  <Route
                    path="/admin-page/all-orders/:orderId"
                    exact
                    component={Order}
                  />
                  <Route
                    path="/admin-page/created-order"
                    exact
                    component={OrderSuccessPage}
                  />
                  <Route component={Error404} />
                </Switch>
              ) : (
                <Switch>
                  <Route path="/" exact component={HomePage} />
                  <Route path="/pizza-builder" exact component={PizzaBuilder} />
                  <Route
                    path="/pizza-builder/:pizzaTemplate/"
                    exact
                    component={PizzaBuilder}
                  />
                  <Route path="/orders/:userId" exact component={Orders} />
                  <Route
                    path="/orders/:userId/:orderId"
                    exact
                    component={Order}
                  />
                  <Route path="/contacts" exact component={Contacts} />
                  <Route
                    path="/created-order"
                    exact
                    component={OrderSuccessPage}
                  />
                  <Route component={Error404} />
                </Switch>
              )}
            </div>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    isLoadingUser: state.auth.isLoadingUser,
    isAuthCheckCompleted: state.auth.isAuthCheckCompleted
  };
};

export default withRouter(
  connect(
    mapStateToProps,
    null
  )(Layout)
);
