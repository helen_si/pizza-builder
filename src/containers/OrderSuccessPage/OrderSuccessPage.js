import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import './OrderSuccessPage.css';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';

class OrderSuccessPage extends Component {
  render() {
    const { user, newOrder } = this.props;
    return (
      <>
        <Helmet>
          <title>Thanks!</title>
        </Helmet>
        {!newOrder ? (
          <Redirect to="/" />
        ) : (
          <div className="OrderSuccessPage">
            <h1>Thanks for your order!</h1>
            <div className="OrderDetailsPosition">
              <div className="OrderDetails">
                <div className="OrderDetailsPosition">
                  <h4>Contact Info:</h4>
                  <div>{newOrder.user.name}</div>
                  <div>{newOrder.user.email}</div>
                  <div>{newOrder.user.phoneNumber}</div>
                  <div>{newOrder.user.address}</div>
                </div>
                <div className="OrderDetailsPosition">
                  <h4>Order:</h4>
                  <div>
                    Pizza with{' '}
                    {Object.keys(newOrder.ingredients).map(
                      (ingredient, index) => {
                        const separator =
                          index !== Object.keys(newOrder.ingredients).length - 1
                            ? ', '
                            : ' ';
                        return (
                          <span key={ingredient} style={{ fontWeight: 'bold' }}>
                            {ingredient}({newOrder.ingredients[ingredient]})
                            {separator}
                          </span>
                        );
                      }
                    )}
                    and{' '}
                    <span style={{ fontWeight: 'bold' }}>
                      {newOrder.sauce}.
                    </span>
                  </div>
                </div>
                <div style={{ fontWeight: 'bold' }}>
                  Payment Method: {newOrder.user.paymentMethod}
                </div>
                <div style={{ fontWeight: 'bold' }}>
                  Total Price: ${newOrder.totalPrice.toFixed(2)}
                </div>
              </div>
            </div>
            <div className="HomePageLink">
              <Link to={user && user.isAdmin ? '/admin-page/all-orders' : '/'}>
                <button
                  className={
                    user && user.isAdmin
                      ? 'Btn AdminBtnColor'
                      : 'Btn UserBtnColor'
                  }
                >
                  Home Page
                </button>
              </Link>
            </div>
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    newOrder: state.orders.newOrder,
    order: state.orders.order,
    isCreatingOrder: state.orders.isCreatingOrder
  };
};

export default connect(
  mapStateToProps,
  null
)(OrderSuccessPage);
