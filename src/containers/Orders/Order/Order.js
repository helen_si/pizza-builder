import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Modal from '../../../components/UI/Modal/Modal';
import OrderForm from '../../../components/OrderForm/OrderForm';
import '../Orders.css';
import moment from 'moment';

class Order extends Component {
  state = {
    isOrdering: false
  };

  componentDidMount() {
    const { getOrder } = this.props;
    const { orderId } = this.props.match.params;
    getOrder(orderId);
  }

  showOrderFormHandler = () => {
    this.setState({ isOrdering: true });
  };

  cancelFormHandler = () => {
    this.setState({ isOrdering: false });
  };

  render() {
    const {
      user,
      isLoadingUser,
      isLoadingOrder,
      order,
      changeOrderStatus
    } = this.props;
    const { isOrdering } = this.state;
    const { orderId } = this.props.match.params;
    let orderDetails = null;
    if (user && order) {
      orderDetails = (
        <div className="Orders">
          <h1>Order Details</h1>
          <div className="OrderDetails">
            <h3>Order №{orderId}</h3>
            <div className="DateStatusRepeatBtn">
              <div
                className={
                  user && user.isAdmin ? 'Date BlueText' : 'Date OrangeText'
                }
              >
                {moment(order.date).format('DD/MM/YYYY HH:mm')}
              </div>
              <div className="RepeatOrder">
                <button
                  className={user.isAdmin ? 'AdminButton' : 'UserButton'}
                  onClick={this.showOrderFormHandler}
                >
                  Repeat Order
                </button>
              </div>
              {user.isAdmin && (
                <div className="OrderStatus">
                  Status:
                  <select
                    defaultValue={order.status}
                    onChange={e => changeOrderStatus(orderId, e.target.value)}
                  >
                    <option value="new">New</option>
                    <option value="preparing">Preparing</option>
                    <option value="done">Done</option>
                    <option value="canceled">Canceled</option>
                  </select>
                </div>
              )}
            </div>
            <div className="ContactInfo">
              <h4>Contact Info:</h4>
              <div>{order.user.name}</div>
              <div>{order.user.email}</div>
              <div>{order.user.phoneNumber}</div>
              <div>{order.user.address}</div>
            </div>
            <h4>Order:</h4>
            <div className="OrderDetails">
              Pizza with{' '}
              {Object.keys(order.ingredients).map((ingredient, index) => {
                const separator =
                  index !== Object.keys(order.ingredients).length - 1
                    ? ', '
                    : ' ';
                return (
                  <span key={ingredient} style={{ fontWeight: 'bold' }}>
                    {ingredient}({order.ingredients[ingredient]}){separator}
                  </span>
                );
              })}
              and <span style={{ fontWeight: 'bold' }}>{order.sauce}.</span>
            </div>
            <div style={{ fontWeight: 'bold' }}>
              Total Price: ${order.totalPrice.toFixed(2)}
            </div>
          </div>
        </div>
      );
    }
    return (
      <>
        <Modal visible={isOrdering} onCancel={this.cancelFormHandler}>
          <OrderForm shouldReset={isOrdering} />
        </Modal>
        {isLoadingUser || isLoadingOrder ? (
          <div className="LoaderContainer">
            <Spinner />
          </div>
        ) : (
          orderDetails
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    isLoadingUser: state.auth.isLoadingUser,
    isLoadingOrder: state.orders.isLoadingOrder,
    order: state.orders.order
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getOrder: orderId => dispatch(actions.getOrder(orderId)),
    changeOrderStatus: (orderId, newStatus) =>
      dispatch(actions.changeOrderStatus(orderId, newStatus))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Order);
