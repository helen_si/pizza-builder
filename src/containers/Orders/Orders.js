import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import Spinner from '../../components/UI/Spinner/Spinner';
import './Orders.css';
import moment from 'moment';

class Orders extends Component {
  componentDidMount() {
    const { getOrders, getAllOrders, user } = this.props;
    if (user && user.isAdmin) {
      getAllOrders();
    } else {
      getOrders();
    }
  }

  render() {
    const { user, isLoadingUser, orders, isLoadingOrders } = this.props;
    let ordersInfo = [];
    if (orders) {
      Object.keys(orders).forEach(orderId => {
        orders[orderId] = {
          ...orders[orderId],
          orderId
        };
        ordersInfo.push(orders[orderId]);
      });
    }
    let ordersDetails = null;
    if (!user) {
      ordersDetails = <Redirect to="/" />;
    } else {
      if (!orders) {
        ordersDetails = <h1>No orders yet!</h1>;
      } else {
        ordersDetails = (
          <div>
            {user.isAdmin ? (
              <h1>Orders</h1>
            ) : (
              <h1>Your orders, {user.name}!</h1>
            )}
            <div className="Orders">
              {ordersInfo.reverse().map(order => {
                return (
                  <React.Fragment key={order.orderId}>
                    {user.isAdmin ? (
                      <Link
                        to={`/admin-page/all-orders/${order.orderId}`}
                        className="Order OrderForAdmin"
                      >
                        <h3>Order №{order.orderId}</h3>
                        <div>
                          Status:{' '}
                          {order.status.charAt(0).toUpperCase() +
                            order.status.slice(1)}
                        </div>
                        <div className="Date BlueText">
                          {moment(order.date).format('D/MM/YYYY HH:mm')}
                        </div>
                      </Link>
                    ) : (
                      <Link
                        to={`/orders/${user.uid}/${order.orderId}`}
                        className="Order OrderForUser"
                      >
                        <h3>Order №{order.orderId}</h3>
                        <div className="Date OrangeText">
                          {moment(order.date).format('D/MM/YYYY HH:mm')}
                        </div>
                      </Link>
                    )}
                  </React.Fragment>
                );
              })}
            </div>
          </div>
        );
      }
    }
    return (
      <>
        <Helmet>
          <title>Orders</title>
        </Helmet>
        {isLoadingUser || isLoadingOrders ? (
          <div className="LoaderContainer">
            <Spinner />
          </div>
        ) : (
          ordersDetails
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    isLoadingUser: state.auth.isLoadingUser,
    orders: state.orders.orders,
    isLoadingOrders: state.orders.isLoadingOrders
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getOrders: () => dispatch(actions.getOrders()),
    getAllOrders: () => dispatch(actions.getAllOrders())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Orders);
