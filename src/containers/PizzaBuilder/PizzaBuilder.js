import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions';
import PizzaItems from '../../components/PizzaItems/PizzaItems';
import Pizza from '../../components/Pizza/Pizza';
import Modal from '../../components/UI/Modal/Modal';
import OrderForm from '../../components/OrderForm/OrderForm';
import PizzaTemplateForm from '../../components/PizzaTemplateForm/PizzaTemplateForm';
import Spinner from '../../components/UI/Spinner/Spinner';
import './PizzaBuilder.css';
import Helmet from 'react-helmet';

class PizzaBuilder extends Component {
  state = {
    isOrdering: false,
    isCreatingPizzaTemplate: false
  };

  componentDidMount() {
    const { fetchPizzaData, cleanOrdersState } = this.props;
    cleanOrdersState();
    fetchPizzaData(this.props.match.params.pizzaTemplate);
  }

  showOrderFormHandler = () => {
    this.setState({ isOrdering: true });
  };

  showPizzaTemplateFormHandler = () => {
    this.setState({ isCreatingPizzaTemplate: true });
  };

  cancelFormHandler = () => {
    this.setState({ isOrdering: false, isCreatingPizzaTemplate: false });
  };

  render() {
    const {
      user,
      ingredients,
      sauces,
      totalPrice,
      basicPrice,
      isLoadingPizzaData,
      addIngredient,
      removeIngredient,
      chooseSauce
    } = this.props;
    const { isOrdering, isCreatingPizzaTemplate } = this.state;
    const pizzaTemplate = this.props.location.state;
    return (
      <>
        <Helmet>
          <title>Pizza Builder</title>
        </Helmet>
        {isLoadingPizzaData ? (
          <div className="LoaderContainer">
            <Spinner />
          </div>
        ) : (
          <div className="PizzaBuilderContainer">
            <Modal visible={isOrdering} onCancel={this.cancelFormHandler}>
              <OrderForm shouldReset={isOrdering} />
            </Modal>
            <Modal
              visible={isCreatingPizzaTemplate}
              onCancel={this.cancelFormHandler}
            >
              <PizzaTemplateForm
                shouldReset={isCreatingPizzaTemplate}
                pizzaTemplate={pizzaTemplate}
              />
            </Modal>
            <h1>Let's make your pizza!</h1>
            <h3>Basic price ${basicPrice && basicPrice.toFixed(2)}</h3>
            <div className="PizzaBuilder">
              <PizzaItems
                ingredients={ingredients}
                sauces={sauces}
                totalPrice={totalPrice}
                basicPrice={basicPrice}
                addIngredient={addIngredient}
                removeIngredient={removeIngredient}
                chooseSauce={chooseSauce}
                showOrderForm={this.showOrderFormHandler}
                showPizzaTemplateForm={this.showPizzaTemplateFormHandler}
                userIsAdmin={user && user.isAdmin ? user.isAdmin : null}
              />
              <Pizza sauces={sauces} ingredients={ingredients} />
            </div>
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user,
    ingredients: state.pizzaData.ingredients,
    sauces: state.pizzaData.sauces,
    totalPrice: state.pizzaData.totalPrice,
    basicPrice: state.pizzaData.basicPrice,
    isLoadingPizzaData: state.pizzaData.isLoadingPizzaData,
    order: state.orders.order
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addIngredient: name => dispatch(actions.addIngredient(name)),
    removeIngredient: name => dispatch(actions.removeIngredient(name)),
    chooseSauce: name => dispatch(actions.chooseSauce(name)),
    fetchPizzaData: pizzaTemplate =>
      dispatch(actions.fetchPizzaData(pizzaTemplate)),
    cleanOrdersState: () => dispatch(actions.cleanOrdersState())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PizzaBuilder);
