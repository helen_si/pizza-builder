import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/auth';
import { loginSuccess, logoutUser } from '../store/actions/auth';

export const initFirebase = store => {
  const config = {
    apiKey: 'AIzaSyC49N23sRFDZr8NcybQiKNZtIxdG0rQWNo',
    authDomain: 'pizza-builder-f5d31.firebaseapp.com',
    databaseURL: 'https://pizza-builder-f5d31.firebaseio.com',
    projectId: 'pizza-builder-f5d31',
    storageBucket: 'pizza-builder-f5d31.appspot.com',
    messagingSenderId: '1059842773329'
  };
  firebase.initializeApp(config);

  firebase.auth().onAuthStateChanged(user => {
    if (user) {
      firebase
        .database()
        .ref(`user-profiles/${user.uid}/`)
        .once('value')
        .then(response => {
          const userInfo = response.val() || {};
          const newUser = {
            ...user,
            ...userInfo
          };
          store.dispatch(loginSuccess(newUser));
        })
        .catch(error => {
          store.dispatch(logoutUser());
        });
    } else {
      store.dispatch(logoutUser());
    }
  });
};
