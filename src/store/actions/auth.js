import * as actionTypes from '../actionTypes/actionTypes';
import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/auth';

const registerRequest = () => ({
  type: actionTypes.REGISTER_REQUEST
});

const registerError = (error) => ({
  type: actionTypes.REGISTER_ERROR,
  error
});

export const registerUser = (name, email, password) => {
  return (dispatch) => {
    dispatch(registerRequest());
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(() => {
        dispatch(setInfo(name, null, null));
      })
      .catch(error => {
        dispatch(registerError(error));
    });
  }  
}

const loginRequest = () => ({
  type: actionTypes.LOGIN_REQUEST
});

export const loginSuccess = (user) => ({
  type: actionTypes.LOGIN_SUCCESS,
  user
});

const loginError = (error) => ({
  type: actionTypes.LOGIN_ERROR,
  error
});

export const loginUser = (email, password) => {
  return (dispatch) => {
    dispatch(loginRequest());
    firebase.auth().signInWithEmailAndPassword(email, password)
      .catch(error => {
        dispatch(loginError(error));
    });
  }  
}

export const logoutUser = () => ({
  type: actionTypes.LOGOUT_USER
});

export const logoutProcess = () => {
  return () => {
    firebase.auth().signOut()
      .then(() => {})
      .catch(() => {});
  }
}

const setInfoRequest = () => ({
  type: actionTypes.SET_INFO_REQUEST,
});

const setInfoSuccess = (userInfo) => ({
  type: actionTypes.SET_INFO_SUCCESS,
  userInfo
});

const setInfoError = (error) => ({
  type: actionTypes.SET_INFO_ERROR,
  error
});

export const setInfo = (name, phoneNumber, address) => {
  return (dispatch) => {
    dispatch(setInfoRequest());
    const user = firebase.auth().currentUser;
    const updates = {};
    const userInfo = {
      name,
      phoneNumber,
      address
    };
    updates['/user-profiles/' + user.uid] = userInfo;
    firebase.database().ref().update(updates, (error) => {
      if (error) {
        dispatch(setInfoError(error));
      } else {
        dispatch(setInfoSuccess(userInfo));
      }
    });
  }  
}
