import * as actionTypes from '../actionTypes/actionTypes';
import firebase from 'firebase/app';
import 'firebase/app';

const getCompanyInfoRequest = () => ({
  type: actionTypes.GET_COMPANY_INFO_REQUEST,
});

const getCompanyInfoSuccess = (companyInfo) => ({
  type: actionTypes.GET_COMPANY_INFO_SUCCESS,
  companyInfo
});

const getCompanyInfoError = (error) => ({
  type: actionTypes.GET_COMPANY_INFO_ERROR,
  error
});

export const getCompanyInfo = () => {
  return (dispatch) => {
    dispatch(getCompanyInfoRequest());
    firebase.database().ref('company-info').once('value')
      .then(response => {
        dispatch(getCompanyInfoSuccess(response.val()));
      })
      .catch(error => {
        dispatch(getCompanyInfoError(error));
      });
  }  
}