import * as actionTypes from '../actionTypes/actionTypes';
import firebase from 'firebase/app';
import 'firebase/app';
import { setInfo } from './auth';

const createOrderRequest = () => ({
  type: actionTypes.CREATE_ORDER_REQUEST,
});

const createOrderSuccess = (newOrder) => ({
  type: actionTypes.CREATE_ORDER_SUCCESS,
  newOrder
});

const createOrderError = (error) => ({
  type: actionTypes.CREATE_ORDER_ERROR,
  error
});

export const createOrder = (newOrder, name, phoneNumber, address) => {
  return (dispatch) => {
    dispatch(createOrderRequest());
    const updates = {};
    const orderKey = firebase.database().ref().child('orders').push().key;
    updates['/orders/' + orderKey] = newOrder;
    let user = null;
    if (firebase.auth().currentUser) {
      const userId = firebase.auth().currentUser.uid;
      firebase.database().ref('user-profiles').child(userId).once('value')
        .then(response => {
          user = response.val();
        });
    }
    firebase.database().ref().update(updates, (error) => {
      if (error) {
        dispatch(createOrderError(error));
      } else {
        firebase.database().ref('orders/' + orderKey).once('value')
          .then(response => {
            dispatch(createOrderSuccess(response.val()));
            if (user && !user.isAdmin) {
              dispatch(setInfo(name, phoneNumber, address));
            }
          })
          .catch(error => {
            dispatch(createOrderError(error));
          });
      }
    });
  }  
}

const getOrdersRequest = () => ({
  type: actionTypes.GET_ORDERS_REQUEST,
});

const getOrdersSuccess = (orders) => ({
  type: actionTypes.GET_ORDERS_SUCCESS,
  orders
});

const getOrdersError = (error) => ({
  type: actionTypes.GET_ORDERS_ERROR,
  error
});

export const getOrders = () => {
  return (dispatch) => {
    dispatch(getOrdersRequest());
    const userId = firebase.auth().currentUser.uid;
    firebase.database().ref('orders').orderByChild('userId').equalTo(userId).once('value')
      .then(response => {
        dispatch(getOrdersSuccess(response.val()));
      })
      .catch(error => {
        dispatch(getOrdersError(error));
      });
  }  
}

const getOrderRequest = () => ({
  type: actionTypes.GET_ORDER_REQUEST,
});

const getOrderSuccess = (order) => ({
  type: actionTypes.GET_ORDER_SUCCESS,
  order
});

const getOrderError = (error) => ({
  type: actionTypes.GET_ORDER_ERROR,
  error
});

export const getOrder = (orderId) => {
  return (dispatch) => {
    dispatch(getOrderRequest());
    firebase.database().ref('orders').child(orderId).once('value')
      .then(response => {
        dispatch(getOrderSuccess(response.val()));
      })
      .catch(error => {
        dispatch(getOrderError(error));
      });
  }  
}

const getAllOrdersRequest = () => ({
  type: actionTypes.GET_ALL_ORDERS_REQUEST,
});

const getAllOrdersSuccess = (orders) => ({
  type: actionTypes.GET_ALL_ORDERS_SUCCESS,
  orders
});

const getAllOrdersError = (error) => ({
  type: actionTypes.GET_ALL_ORDERS_ERROR,
  error
});

export const getAllOrders = () => {
  return (dispatch) => {
    dispatch(getAllOrdersRequest());
    firebase.database().ref('orders').once('value')
      .then(response => {
        dispatch(getAllOrdersSuccess(response.val()));
      })
      .catch(error => {
        dispatch(getAllOrdersError(error));
      });
  }  
}

const changeOrderStatusRequest = () => ({
  type: actionTypes.CHANGE_ORDER_STATUS_REQUEST,
});

const changeOrderStatusSuccess = (order) => ({
  type: actionTypes.CHANGE_ORDER_STATUS_SUCCESS,
  order
});

const changeOrderStatusError = (error) => ({
  type: actionTypes.CHANGE_ORDER_STATUS_ERROR,
  error
});

export const changeOrderStatus = (orderId, newStatus) => {
  return (dispatch) => {
    dispatch(changeOrderStatusRequest());
    firebase.database().ref().update({ [`orders/${orderId}/status`]: newStatus });
    firebase.database().ref('orders').child(orderId).once('value')
      .then(response => {
        dispatch(changeOrderStatusSuccess(response.val()));
      })
      .catch(error => {
        dispatch(changeOrderStatusError(error));
      });
  }  
}

export const cleanOrdersState = () => ({
  type: actionTypes.CLEAN_ORDERS_STATE
});
