import * as actionTypes from '../actionTypes/actionTypes';
import firebase from 'firebase/app';
import 'firebase/app';
import 'firebase/database';

export const addIngredient = (name) => ({
    type: actionTypes.ADD_INGREDIENT,
    name
  });

export const removeIngredient = (name) => ({
  type: actionTypes.REMOVE_INGREDIENT,
  name
});

export const chooseSauce = (name) => ({
  type: actionTypes.CHOOSE_SAUCE,
  name
});

const fetchPizzaDataRequest = () => ({
  type: actionTypes.PIZZA_DATA_REQUEST
});

const fetchPizzaDataSuccess = (pizzaData) => ({
  type: actionTypes.PIZZA_DATA_SUCCESS,
  pizzaData
});

const fetchPizzaDataError = (error) => ({
  type: actionTypes.PIZZA_DATA_ERROR,
  error
});

export const fetchPizzaData = (pizzaTemplate) => {
  return (dispatch) =>  {
    dispatch(fetchPizzaDataRequest());
    let pizzaTemplateData = null;
    let pizzaBasicData = null;
    if (pizzaTemplate) {
      firebase.database().ref('pizza-templates').child(pizzaTemplate).once('value')
      .then(response => {
        pizzaTemplateData = response.val();
      })
      .catch(error => {
        dispatch(fetchPizzaDataError(error));
      });
    }
    firebase.database().ref('pizza-data').once('value')
      .then(response => {
        pizzaBasicData = response.val();
        const pizzaData = {
          pizzaBasicData,
          pizzaTemplateData
        };
        dispatch(fetchPizzaDataSuccess(pizzaData));
      })
      .catch(error => {
        dispatch(fetchPizzaDataError(error));
      });
  }
}

const getPizzaTemplatesRequest = () => ({
  type: actionTypes.PIZZA_TEMPLATES_REQUEST
});

const getPizzaTemplatesSuccess = (pizzaTemplates) => ({
  type: actionTypes.PIZZA_TEMPLATES_SUCCESS,
  pizzaTemplates
});

const getPizzaTemplatesError = (error) => ({
  type: actionTypes.PIZZA_TEMPLATES_ERROR,
  error
});

export const getPizzaTemplates = () => {
  return (dispatch) =>  {
    dispatch(getPizzaTemplatesRequest());
    firebase.database().ref('pizza-templates').once('value')
      .then(response => {
        dispatch(getPizzaTemplatesSuccess(response.val()));
      })
      .catch(error => {
        dispatch(getPizzaTemplatesError(error));
      });
  }
}

const createPizzaTemplateRequest = () => ({
  type: actionTypes.CREATE_PIZZA_TEMPLATE_REQUEST
});

const createPizzaTemplateSuccess = (pizzaTemplate) => ({
  type: actionTypes.CREATE_PIZZA_TEMPLATE_SUCCESS,
  pizzaTemplate
});

const createPizzaTemplateError = (error) => ({
  type: actionTypes.CREATE_PIZZA_TEMPLATE_ERROR,
  error
});

export const createPizzaTemplate = (pizzaTemplate) => {
  return (dispatch) =>  {
    dispatch(createPizzaTemplateRequest());
    const updates = {};
    const pizzaTemplateKey = firebase.database().ref().child('orders').push().key;
    updates['/pizza-templates/' + pizzaTemplateKey] = pizzaTemplate;
    firebase.database().ref().update(updates, (error) => {
      if (error) {
        dispatch(createPizzaTemplateError(error));
      } else {
        firebase.database().ref('pizza-templates/' + pizzaTemplateKey).once('value')
          .then(response => {
            dispatch(createPizzaTemplateSuccess(response.val()));
          })
          .catch(error => {
            dispatch(createPizzaTemplateError(error));
          });
      }
    });
  }
}
