import * as actionTypes from '../actionTypes/actionTypes';

const initialState = {
  user: null,
  error: null,
  isLoadingUser: false,
  isAuthCheckCompleted: false
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.REGISTER_REQUEST: {
      return {
        ...state,
        user: null,
        error: null,
        isLoadingUser: true
      };
    }
    case actionTypes.REGISTER_ERROR: {
      return {
        ...state,
        error: action.error,
        isLoadingUser: false
      };
    }
    case actionTypes.LOGIN_REQUEST: {
      return {
        ...state,
        user: null,
        error: null,
        isLoadingUser: true
      };
    }
    case actionTypes.LOGIN_SUCCESS: {
      return {
        ...state,
        user: action.user,
        isLoadingUser: false,
        isAuthCheckCompleted: true
      };
    }
    case actionTypes.LOGIN_ERROR: {
      return {
        ...state,
        error: action.error,
        isLoadingUser: false
      };
    }
    case actionTypes.LOGOUT_USER: {
      return {
        ...state,
        user: null,
        isLoadingUser: false,
        isAuthCheckCompleted: true
      };
    }
    case actionTypes.SET_INFO_REQUEST: {
      return {
        ...state,
        error: null,
        isLoadingUser: true
      };
    }
    case actionTypes.SET_INFO_SUCCESS: {
      const updatedUser = {
        ...state.user,
        ...action.userInfo
      };
      return {
        ...state,
        user: updatedUser,
        isLoadingUser: false
      };
    }
    case actionTypes.SET_INFO_ERROR: {
      return {
        ...state,
        error: action.error,
        isLoadingUser: false
      };
    }
    default:
      return state;
  }
};

export default auth;
