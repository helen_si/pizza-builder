import * as actionTypes from '../actionTypes/actionTypes';

const initialState = {
  companyInfo: null,
  error: null,
  isLoadingCompanyInfo: false
};

const companyInfo = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_COMPANY_INFO_REQUEST: {
      return {
        ...state,
        companyInfo: null,
        error: null,
        isLoadingCompanyInfo: true
      };
    }
    case actionTypes.GET_COMPANY_INFO_SUCCESS: {
      return {
        ...state,
        companyInfo: action.companyInfo,
        isLoadingCompanyInfo: false
      };
    }
    case actionTypes.GET_COMPANY_INFO_ERROR: {
      return {
        ...state,
        error: action.error,
        isLoadingCompanyInfo: false
      };
    }
    default:
      return state;
  }
};

export default companyInfo;
