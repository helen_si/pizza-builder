import * as actionTypes from '../actionTypes/actionTypes';

const initialState = {
  user: null,
  userId: null,
  ingredients: null,
  sauce: null,
  totalPrice: null,
  date: null,
  error: null,
  isCreatingOrder: false,
  isLoadingOrders: false,
  orders: null,
  isLoadingOrder: false,
  order: null,
  newOrder: null
};

const orders = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CREATE_ORDER_REQUEST: {
      return {
        ...state,
        user: null,
        userId: null,
        ingredients: null,
        sauce: null,
        totalPrice: null,
        date: null,
        status: null,
        error: null,
        isCreatingOrder: true,
        newOrder: null
      };
    }
    case actionTypes.CREATE_ORDER_SUCCESS: {
      const { ingredients, sauce, totalPrice, date, user, userId, status } = action.newOrder;
      return {
        ...state,
        user,
        userId,
        ingredients,
        sauce,
        totalPrice,
        date,
        status,
        isCreatingOrder: false,
        newOrder: action.newOrder
      };
    }
    case actionTypes.CREATE_ORDER_ERROR: {
      return {
        ...state,
        error: action.error,
        isCreatingOrder: false
      };
    }
    case actionTypes.GET_ORDERS_REQUEST: {
      return {
        ...state,
        error: null,
        isLoadingOrders: true,
        orders: null
      };
    }
    case actionTypes.GET_ORDERS_SUCCESS: {
      return {
        ...state,
        isLoadingOrders: false,
        orders: action.orders
      };
    }
    case actionTypes.GET_ORDERS_ERROR: {
      return {
        ...state,
        isLoadingOrders: false,
        error: action.error
      };
    }
    case actionTypes.GET_ORDER_REQUEST: {
      return {
        ...state,
        error: null,
        isLoadingOrder: true,
        order: null
      };
    }
    case actionTypes.GET_ORDER_SUCCESS: {
      return {
        ...state,
        isLoadingOrder: false,
        order: action.order
      };
    }
    case actionTypes.GET_ORDER_ERROR: {
      return {
        ...state,
        isLoadingOrder: false,
        error: action.error
      };
    }
    case actionTypes.GET_ALL_ORDERS_REQUEST: {
      return {
        ...state,
        error: null,
        isLoadingOrders: true,
        orders: null
      };
    }
    case actionTypes.GET_ALL_ORDERS_SUCCESS: {
      return {
        ...state,
        isLoadingOrders: false,
        orders: action.orders
      };
    }
    case actionTypes.GET_ALL_ORDERS_ERROR: {
      return {
        ...state,
        isLoadingOrders: false,
        error: action.error
      };
    }
    case actionTypes.CHANGE_ORDER_STATUS_REQUEST: {
      return {
        ...state,
        error: null,
        isLoadingOrder: true,
        order: null
      };
    }
    case actionTypes.CHANGE_ORDER_STATUS_SUCCESS: {
      return {
        ...state,
        isLoadingOrder: false,
        order: action.order
      };
    }
    case actionTypes.CHANGE_ORDER_STATUS_ERROR: {
      return {
        ...state,
        isLoadingOrder: false,
        error: action.error
      };
    }
    case actionTypes.CLEAN_ORDERS_STATE: {
      return {
        ...state,
        order: null,
        orders: null,
        newOrder: null
      };
    }
    default:
      return state;
  }
};

export default orders;
