import * as actionTypes from '../actionTypes/actionTypes';

const initialState = {
  ingredients: [],
  sauces: [],
  totalPrice: null,
  basicPrice: null,
  error: null,
  isLoadingPizzaData: false,
  pizzaTemplates: null,
  pizzaTemplate: null,
  isCreatingPizzaTemplate: false,
}

const pizzaData = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.PIZZA_DATA_REQUEST: {
      return {
        ...state,
        ingredients: [],
        sauces: [],
        totalPrice: null,
        basicPrice: null,
        error: null,
        isLoadingPizzaData: true
      };
    }
    case actionTypes.PIZZA_DATA_SUCCESS: {
      const { pizzaBasicData, pizzaTemplateData } = action.pizzaData;
      const { ingredients, sauces, totalPrice, basicPrice } = {...pizzaBasicData};
      const updatedPizzaData = {
        ingredients,
        sauces,
        totalPrice,
      };
      if (pizzaTemplateData) {
        pizzaTemplateData.ingredients.forEach(templateIngredient => {
          updatedPizzaData.ingredients.forEach(ingredient => {
            if (ingredient.name === templateIngredient.name) {
              ingredient.amount += templateIngredient.amount;
              updatedPizzaData.totalPrice += ingredient.price * ingredient.amount;
            }
          });
        });
        pizzaTemplateData.sauce.forEach(templateSauce => {
          updatedPizzaData.sauces.forEach(sauce => {
            sauce.selected = false;
            if (sauce.name === templateSauce.name) {
              sauce.selected = true;
            }
          });
        });
      }
      return {
        ...state,
        isLoadingPizzaData: false,
        ingredients: updatedPizzaData.ingredients,
        sauces: updatedPizzaData.sauces,
        totalPrice: updatedPizzaData.totalPrice,
        basicPrice
      };
    }
    case actionTypes.PIZZA_DATA_ERROR: {
      return {
        ...state,
        isLoadingPizzaData: false,
        error: action.error
      }
    }
    case actionTypes.ADD_INGREDIENT: {
      const { name } = action;
      const updatedIngredients = [
        ...state.ingredients
      ];
      const updatedTotalPrice = state.totalPrice;
      const idx = updatedIngredients.findIndex((item) => item.name === name);
      updatedIngredients[idx].amount += 1;
      return {
        ...state,
        ingredients: updatedIngredients,
        totalPrice: updatedTotalPrice + state.ingredients[idx].price,
      };
    }
    case actionTypes.REMOVE_INGREDIENT: {
      const { name } = action;
      const updatedIngredients = [
        ...state.ingredients
      ];
      const updatedTotalPrice = state.totalPrice;
      const idx = updatedIngredients.findIndex((item) => item.name === name);
      updatedIngredients[idx].amount -= 1;
      return {
        ...state,
        ingredients: updatedIngredients,
        totalPrice: updatedTotalPrice - state.ingredients[idx].price
      };
    }
    case actionTypes.CHOOSE_SAUCE: {
      const { name } = action;
      const updatedSauces = [
        ...state.sauces
      ];
      const idx = updatedSauces.findIndex((item) => item.name === name);
      updatedSauces.forEach((item, i) => {
        item.selected = i === idx;
      });
      return {
        ...state,
        sauces: updatedSauces,
      };
    }
    case actionTypes.PIZZA_TEMPLATES_REQUEST: {
      return {
        ...state,
        error: null,
        isLoadingPizzaData: true,
        pizzaTemplates: null
      };
    }
    case actionTypes.PIZZA_TEMPLATES_SUCCESS: {
      return {
        ...state,
        isLoadingPizzaData: false,
        pizzaTemplates: action.pizzaTemplates
      };
    }
    case actionTypes.PIZZA_TEMPLATES_ERROR: {
      return {
        ...state,
        isLoadingPizzaData: false,
        error: action.error
      }
    }
    case actionTypes.CREATE_PIZZA_TEMPLATE_REQUEST: {
      return {
        ...state,
        error: null,
        pizzaTemplate: null,
        isCreatingPizzaTemplate: true
      };
    }
    case actionTypes.CREATE_PIZZA_TEMPLATE_SUCCESS: {
      return {
        ...state,
        isCreatingPizzaTemplate: false,
        pizzaTemplate: action.pizzaTemplate
      };
    }
    case actionTypes.CREATE_PIZZA_TEMPLATE_ERROR: {
      return {
        ...state,
        isCreatingPizzaTemplate: false,
        error: action.error
      }
    }
    default:
      return state;
  }
};

export default pizzaData;
