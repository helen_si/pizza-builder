import { combineReducers } from 'redux';
import pizzaData from './pizzaData';
import auth from './auth';
import orders from './orders';
import companyInfo from './companyInfo';

const reducer = combineReducers({
  pizzaData,
  auth,
  orders,
  companyInfo
});

export default reducer;
