import { createStore, applyMiddleware, compose } from 'redux';
import reducer from './reducers/reducer';
import thunk from 'redux-thunk';
import { initFirebase } from '../firebase';

const middleware = applyMiddleware(thunk);

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window !== 'undefined' &&
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
    : compose;

const store = createStore(reducer, composeEnhancers(middleware));

initFirebase(store);

export default store;
